# MyJLPApplication by Xu

## Overview
This project was created primarily for a JLP interview assignment.

The app has been designed with two main pages: one displaying the list of Dishwashers from the search result, 
and another presenting the detailed information of the clicked product.

## Introduction
In this assignment, I utilized the following technologies:

1. The project is designed based on Clean Architecture and follows the MVVM pattern.
2. Kotlin coroutines and Jetpack Compose are used for asynchronous programming, providing more expressive UIs.
3. Dagger Hilt is utilized for dependency injection, improving code maintainability and testability.
4. The third-party library Glide is integrated for efficient caching and display of images, ensuring a smooth image experience.
5. Unit tests have been implemented for necessary classes to guarantee code quality and reliability.

## Assignment Requirement Analysis
1. UI analysis ->
In accordance with the design pictures and specifications, the app necessitates two primary pages: 
the Dishwasher list page and the Dishwasher detail page. To address various scenarios, I intend to create four distinct UI states:

    A. Loading State: Display a loading screen while fetching data from the network.
    B. Error State: Present an error screen to guide users in handling error messages.
    C. Product List State: Showcase the Dishwasher list page and implement LazyVerticalGrid to achieve a grid-style presentation of products.
    D. Product Detail State: Reveal the Dishwasher detail page and include a back button for navigation back to the list page.

Regarding navigation between the Product List screen and Product Detail screen, I have implemented the necessary mechanisms. 
To ensure the app works seamlessly across both phones and tablets, I've incorporated two methods: One method indicates whether 
the device is a tablet or a phone. Another method signifies whether the device is in landscape or portrait orientation. These 
methods dynamically adjust the layout of the screen, adhering to the assignment requirements.

2. Data analysis ->
Based on the provided JSON data, I've designed the data model, and I've chosen Retrofit to handle network requests. Since the assignment 
doesn't explicitly mention a database requirement, I've focused solely on implementing the network part, retrieving all data via network requests.

In addition, it's important to highlight that the data format in the provided JSON differs from the format acquired through network requests.
This variance has introduced a slight delay in my progress. Furthermore, I've had to include specific "Header" information to successfully request
data from the URL.

If there's a need to implement a database, I'd prefer to allocate more time to development, especially considering the complexity of the data structure. 
I'd also like to discuss the entity structure with some of your team members.

In the project, I've created two models: one for retrieving data via Retrofit in the data module and another for displaying data in the UI in the domain module.

3. Approach analysis ->
Based on the analysis provided, I've structured the project into four distinct modules:
    A. Core Module:
        Overview: This module is designed to handle complex logic that spans across other modules.
        Dependencies: None mentioned; potentially serves as a foundational module.
    B. Data Module:
        Overview: Responsible for data management, including handling data via network (Retrofit) or local storage (database).  
        Dependencies: Potentially depends on the Core Module for shared logic.
    C. Domain Module:
        Overview: Focuses on the most critical logical methods, acting as a bridge between data and the main (UI) module.
        Dependencies: Depends on the Data Module for data-related functionalities and may depend on the Core Module for shared logic.
    D. Main (Application):
        Overview: Manages the UI aspects of the application. 
        Dependencies: Depends on the Domain Module for logical operations and may depend on the Core Module for shared logic.

For the UI components, I've initially crafted four UI states and will be implementing two ViewModels for the two pages.
Each page encompasses various composable screens, and StateFlow has been employed in the ViewModels. During the implementation,
I identified certain data that needs to be shared between the two ViewModels, such as the message "Claim a free extra 3-year
guarantee (Via redemption)." Anticipating more shared data between the ViewModels, I've established a singleton SharedData
object to facilitate data sharing across screens.

## Installation
Since this project is only for the interview porpers, I have not implement proguard and signingConfigs. You can install the app by debug model

## Usage
This project is only can be used by JLP development group and Xu

## Contributing
None

## License
MIT License

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
=======
# JLP_Interview_xu



## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/xuzhang2015/jlp_interview_xu.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/xuzhang2015/jlp_interview_xu/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Set auto-merge](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing (SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!). Thanks to [makeareadme.com](https://www.makeareadme.com/) for this template.

## Suggestions for a good README

Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.

## Name
Choose a self-explaining name for your project.

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.


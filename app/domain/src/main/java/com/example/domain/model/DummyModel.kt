package com.example.domain.model

import com.example.domain.Ulitis.ADJUSTABLE_RACKING_INFORMATION_DISPLAY
import com.example.domain.Ulitis.CHILD_LOCK_DOOR_DISPLAY
import com.example.domain.Ulitis.DELAY_WASH_DISPLAY
import com.example.domain.Ulitis.DELICATE_WASH_DISPLAY
import com.example.domain.Ulitis.DIMENSIONS_DISPLAY
import com.example.domain.Ulitis.DRYING_PERFORMANCE_DISPLAY
import com.example.domain.Ulitis.DRYING_SYSTEM_DISPLAY
import com.example.domain.Ulitis.GUAEANTEE_DISPLAY

object DummyModel {

    val dummyProductId = "110488704"

    val dummyProductInforMessages = listOf(
        "Claim a free extra 3 year guarantee (Via redemption)"
    )

    val dummyProductInfoModel = ProductInfoModel(
        productId = dummyProductId,
        title = "Bosch Series 4 SMS4HMW00G Freestanding Dishwasher, White",
        imageUrl = "https://johnlewis.scene7.com/is/image/JohnLewis/234378764?",
        maxPrice = "£549.00",
        minPrice = "£549.00",
        messages = dummyProductInforMessages
    )

    val dummySharedDataModel = SharedDataModel(
        productId = dummyProductId,
        price = "£549.00",
        messages = dummyProductInforMessages
    )

    val dummyProductInfoModelList = listOf(
        dummyProductInfoModel,
        dummyProductInfoModel,
        dummyProductInfoModel,
        dummyProductInfoModel,
        dummyProductInfoModel,
        dummyProductInfoModel,
        dummyProductInfoModel,
        dummyProductInfoModel,
        dummyProductInfoModel,
        dummyProductInfoModel,
        dummyProductInfoModel,
        dummyProductInfoModel,
        dummyProductInfoModel,
        dummyProductInfoModel,
        dummyProductInfoModel,
        dummyProductInfoModel,
        dummyProductInfoModel,
        dummyProductInfoModel,
        dummyProductInfoModel,
        dummyProductInfoModel,
        dummyProductInfoModel,
        dummyProductInfoModel,
        dummyProductInfoModel,
        dummyProductInfoModel,
    )

    val dummyImageUrls = listOf(
        "https://johnlewis.scene7.com/is/image/JohnLewis/110429243?",
        "https://johnlewis.scene7.com/is/image/JohnLewis/110429243alt1?",
        "https://johnlewis.scene7.com/is/image/JohnLewis/110429243alt2?",
        "https://johnlewis.scene7.com/is/image/JohnLewis/110429243alt3?",
        "https://johnlewis.scene7.com/is/image/JohnLewis/110429243alt4?",
        "https://johnlewis.scene7.com/is/image/JohnLewis/110429243alt5?",
        "https://johnlewis.scene7.com/is/image/JohnLewis/110429243alt6?",
        "https://johnlewis.scene7.com/is/image/JohnLewis/110429243alt10?",
    )

    val dummyProductFeatures = mapOf(
        GUAEANTEE_DISPLAY to "2 year guarantee included",
        ADJUSTABLE_RACKING_INFORMATION_DISPLAY to "Yes",
        CHILD_LOCK_DOOR_DISPLAY to "No",
        DELAY_WASH_DISPLAY to "Yes - up to 24 hours",
        DELICATE_WASH_DISPLAY to "Yes",
        DIMENSIONS_DISPLAY to "H84.5 x W60 x D60cm",
        DRYING_PERFORMANCE_DISPLAY to "B",
        DRYING_SYSTEM_DISPLAY to "Heat Exchanger",
    )

    val dummyProductDetailModel = ProductDetailModel(
        title = "Bosch Series 4 SMS4HMW00G Freestanding Dishwasher, White",
        code = "81701204",
        price = "£456.00",
        productInformation = "<p>Forget slaving over the washing up and missing out on family time. Bosch helps keep your worktops free from clutter thanks to the spacious interior and variety of cleaning options they've packed into this dishwasher. The 60 minute quick wash cycle is really handy, especially if you forget the kids are having their friends over for tea, and the adjustable racking means there's space for any larger items that need a clean too. Plus, Heat Exchanger drying gets rid of moisture with warmth from the wash, for cupboard-ready glasses and cutlery every time.</p>",
        features = dummyProductFeatures,
        messages = dummyProductInforMessages,
        imageUrls = dummyImageUrls
    )


}
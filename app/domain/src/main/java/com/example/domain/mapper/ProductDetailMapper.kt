package com.example.domain.mapper

import com.example.data.model.productDetail.ProductDetails
import com.example.data.model.productDetail.ProductItemDetail
import com.example.data.model.productDetail.ProductMedia
import com.example.domain.Ulitis.ADJUSTABLE_RACKING_INFORMATION
import com.example.domain.Ulitis.ADJUSTABLE_RACKING_INFORMATION_DISPLAY
import com.example.domain.Ulitis.CHILD_LOCK_DOOR
import com.example.domain.Ulitis.CHILD_LOCK_DOOR_DISPLAY
import com.example.domain.Ulitis.DELAY_WASH
import com.example.domain.Ulitis.DELAY_WASH_DISPLAY
import com.example.domain.Ulitis.DELICATE_WASH
import com.example.domain.Ulitis.DELICATE_WASH_DISPLAY
import com.example.domain.Ulitis.DIMENSIONS
import com.example.domain.Ulitis.DIMENSIONS_DISPLAY
import com.example.domain.Ulitis.DRYING_PERFORMANCE
import com.example.domain.Ulitis.DRYING_PERFORMANCE_DISPLAY
import com.example.domain.Ulitis.DRYING_SYSTEM
import com.example.domain.Ulitis.DRYING_SYSTEM_DISPLAY
import com.example.domain.Ulitis.GUAEANTEE
import com.example.domain.Ulitis.GUAEANTEE_DISPLAY
import com.example.domain.model.ProductDetailModel
import javax.inject.Inject

class ProductDetailMapper @Inject constructor() : IMapper<ProductItemDetail, ProductDetailModel> {
    override fun covertEntityToModel(entity: ProductItemDetail): ProductDetailModel {
        return ProductDetailModel(
            title = entity.title,
            code = entity.code,
            price = "",
            productInformation = entity.details.productInformation,
            features = filterNecessaryFeatures(entity.details),
            imageUrls = getImageUrs(entity.media)
        )
    }

    fun getImageUrs(media: ProductMedia): List<String> {
        return media.images.urls.map {
            "https:$it"
        }
    }

    fun filterNecessaryFeatures(data: ProductDetails): Map<String, String> {
        val features = data.features
        if (features.isEmpty() || features[0].attributes.isEmpty()) {
            return emptyMap()
        }

        val attributeMappings = mapOf(
            GUAEANTEE to GUAEANTEE_DISPLAY,
            ADJUSTABLE_RACKING_INFORMATION to ADJUSTABLE_RACKING_INFORMATION_DISPLAY,
            CHILD_LOCK_DOOR to CHILD_LOCK_DOOR_DISPLAY,
            DELAY_WASH to DELAY_WASH_DISPLAY,
            DELICATE_WASH to DELICATE_WASH_DISPLAY,
            DIMENSIONS to DIMENSIONS_DISPLAY,
            DRYING_PERFORMANCE to DRYING_PERFORMANCE_DISPLAY,
            DRYING_SYSTEM to DRYING_SYSTEM_DISPLAY
        )

        return features[0].attributes
            .filter { it.name in attributeMappings }
            .associate { attribute ->
                val displayName = attributeMappings[attribute.name]
                displayName?.let {
                    it to
                            if (attribute.multivalued) {
                                "Yes"
                            } else {
                                attribute.value
                            }
                }!!
            }
    }
}
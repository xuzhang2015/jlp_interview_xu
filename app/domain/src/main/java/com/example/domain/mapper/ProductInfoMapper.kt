package com.example.domain.mapper

import com.example.data.model.productList.ProductInfo
import com.example.data.model.productList.ProductMessage
import com.example.domain.model.ProductInfoModel
import javax.inject.Inject

class ProductInfoMapper @Inject constructor() : IMapper<ProductInfo, ProductInfoModel> {
    override fun covertEntityToModel(entity: ProductInfo): ProductInfoModel {
        return ProductInfoModel(
            productId = entity.productId,
            title = entity.title,
            imageUrl = "https:" + entity.image,
            maxPrice = entity.variantPriceRange.display.max,
            minPrice = entity.variantPriceRange.display.min,
            messages = getProductMessage(entity.messaging),
        )
    }

    fun getProductMessage(productMessage: List<ProductMessage>): List<String> {
        return productMessage.map { it.title }
    }
}
package com.example.domain.usercase

import com.example.domain.model.ProductInfoModel
import com.example.domain.repository.IRepository
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class GetProductInfoListUserCase @Inject constructor(
    private val productListRepository: IRepository<Unit, ProductInfoModel>
) : IUserCase<Unit, ProductInfoModel> {

    override fun execute(parameter: Unit): Flow<List<ProductInfoModel>> {
        return productListRepository.getData(parameter)
    }
}
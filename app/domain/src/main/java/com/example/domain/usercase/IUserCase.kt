package com.example.domain.usercase

import kotlinx.coroutines.flow.Flow

interface IUserCase<V, T> {
    fun execute(parameter: V): Flow<List<T>>
}
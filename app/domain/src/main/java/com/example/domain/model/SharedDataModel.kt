package com.example.domain.model

data class SharedDataModel(
    val productId: String = "",
    val price: String = "",
    val messages: List<String> = listOf()
)

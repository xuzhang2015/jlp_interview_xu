package com.example.domain.repository

import com.example.data.datasource.INetworkDataSource
import com.example.data.model.productList.ProductInfo
import com.example.domain.mapper.IMapper
import com.example.domain.model.ProductInfoModel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flatMapConcat
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class GetProductListRepository @Inject constructor(
    private val networkDataSource: INetworkDataSource,
    private val productInfoMapper: IMapper<ProductInfo, ProductInfoModel>
) : IRepository<Unit, ProductInfoModel> {

    override fun getData(parameter: Unit): Flow<List<ProductInfoModel>> {
        return networkDataSource.requestSearchProductList().flatMapConcat { data ->
            flow {
                emit(
                    data.map(productInfoMapper::covertEntityToModel)
                )
            }
        }
    }
}
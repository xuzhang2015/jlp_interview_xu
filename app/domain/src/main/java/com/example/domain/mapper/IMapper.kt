package com.example.domain.mapper

interface IMapper<E, M> {
    fun covertEntityToModel(entity: E): M
}
package com.example.domain.model

data class ProductDetailModel(
    val title: String,
    val code: String,
    val price: String,
    val productInformation: String,
    val features: Map<String, String> = mapOf(),
    val messages: List<String> = listOf(),
    val imageUrls: List<String> = listOf(),
)

package com.example.domain.di

import com.example.data.model.productDetail.ProductItemDetail
import com.example.data.model.productList.ProductInfo
import com.example.domain.mapper.IMapper
import com.example.domain.mapper.ProductDetailMapper
import com.example.domain.mapper.ProductInfoMapper
import com.example.domain.model.ProductDetailModel
import com.example.domain.model.ProductInfoModel
import com.example.domain.repository.GetProductDetailRepository
import com.example.domain.repository.GetProductListRepository
import com.example.domain.repository.IRepository
import com.example.domain.usercase.GetProductDetailUserCase
import com.example.domain.usercase.GetProductInfoListUserCase
import com.example.domain.usercase.IUserCase
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
abstract class DomainModule {
    @Binds
    @Singleton
    abstract fun bindGetProductDetailRepository(getProductDetailRepository: GetProductDetailRepository): IRepository<String, ProductDetailModel>

    @Binds
    @Singleton
    abstract fun bindGetProductListRepository(getProductListRepository: GetProductListRepository): IRepository<Unit, ProductInfoModel>

    @Binds
    @Singleton
    abstract fun bindProductInfoMapper(productInfoMapper: ProductInfoMapper): IMapper<ProductInfo, ProductInfoModel>

    @Binds
    @Singleton
    abstract fun bindProductDetailMapper(productDetailMapper: ProductDetailMapper): IMapper<ProductItemDetail, ProductDetailModel>

    @Binds
    @Singleton
    abstract fun bindGetProductInfoListUserCase(getProductInfoListUserCase: GetProductInfoListUserCase): IUserCase<Unit, ProductInfoModel>

    @Binds
    @Singleton
    abstract fun bindGetProductDetailUserCase(getProductDetailUserCase: GetProductDetailUserCase): IUserCase<String, ProductDetailModel>
}
package com.example.domain.repository

import com.example.data.datasource.INetworkDataSource
import com.example.data.model.productDetail.ProductItemDetail
import com.example.domain.mapper.IMapper
import com.example.domain.model.ProductDetailModel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flatMapConcat
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class GetProductDetailRepository @Inject constructor(
    private val networkDataSource: INetworkDataSource,
    private val productDetailMapper: IMapper<ProductItemDetail, ProductDetailModel>
) : IRepository<String, ProductDetailModel> {

    override fun getData(parameter: String): Flow<List<ProductDetailModel>> {
        return networkDataSource.requestProductDetail(parameter).flatMapConcat { data ->
            flow {
                val model = productDetailMapper.covertEntityToModel(data)
                emit(
                    listOf(model)
                )
            }
        }
    }
}
package com.example.domain.repository

import kotlinx.coroutines.flow.Flow

interface IRepository<V, T> {
    fun getData(parameter: V): Flow<List<T>>
}
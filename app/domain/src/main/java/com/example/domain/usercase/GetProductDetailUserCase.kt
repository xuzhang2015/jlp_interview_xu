package com.example.domain.usercase

import com.example.domain.model.ProductDetailModel
import com.example.domain.repository.IRepository
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class GetProductDetailUserCase @Inject constructor(
    private val productDetailRepository: IRepository<String, ProductDetailModel>
) : IUserCase<String, ProductDetailModel> {

    override fun execute(parameter: String): Flow<List<ProductDetailModel>> {
        return productDetailRepository.getData(parameter)
    }
}
package com.example.domain

object Ulitis {
    const val GUAEANTEE = "Guarantee"
    const val GUAEANTEE_DISPLAY = "Guarantee"
    const val ADJUSTABLE_RACKING_INFORMATION = "Adjustable"
    const val ADJUSTABLE_RACKING_INFORMATION_DISPLAY = "Adjustable racking Information"
    const val CHILD_LOCK_DOOR = "Child lock"
    const val CHILD_LOCK_DOOR_DISPLAY = "Child lock Door && control lock Delay Start"
    const val DELAY_WASH = "Delay Start"
    const val DELAY_WASH_DISPLAY = "Delay Wash"
    const val DELICATE_WASH = "Delicate Wash"
    const val DELICATE_WASH_DISPLAY = "Delicate Wash"
    const val DIMENSIONS = "Dimensions"
    const val DIMENSIONS_DISPLAY = "Dimensions"
    const val DRYING_PERFORMANCE = "Drying performance"
    const val DRYING_PERFORMANCE_DISPLAY = "Drying performance"
    const val DRYING_SYSTEM = "Drying system"
    const val DRYING_SYSTEM_DISPLAY = "Drying system"
}
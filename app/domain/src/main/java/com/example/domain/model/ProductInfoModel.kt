package com.example.domain.model

data class ProductInfoModel(
    val productId: String,
    val title: String,
    val imageUrl: String,
    val maxPrice: String,
    val minPrice: String,
    val messages: List<String> = listOf(),
)

package com.example.domain.mapper


import com.example.data.model.productList.ProductInfo
import com.example.data.model.productList.ProductMessage
import com.example.data.model.productList.ProductPriceDisplay
import com.example.data.model.productList.ProductPriceValue
import com.example.data.model.productList.VariantPriceRange
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test

class ProductInfoMapperTest {
    private lateinit var mapper: ProductInfoMapper

    @Before
    fun setup() {
        mapper = ProductInfoMapper()
    }

    @Test
    fun `covertEntityToModel should map ProductInfo to ProductInfoModel`() {
        val mockProductInfo = ProductInfo(
            productId = "123",
            title = "Test Product",
            image = "/images/test.jpg",
            variantPriceRange = VariantPriceRange(
                display = ProductPriceDisplay(
                    min = "10.0",
                    max = "20.0"
                ),
                value = ProductPriceValue(
                    10F,
                    20F,
                )
            ),
            messaging = listOf(
                ProductMessage(
                    productId = "123",
                    title = "Message 1"
                ),
                ProductMessage(
                    productId = "123",
                    title = "Message 2"
                )
            )
        )

        val result = mapper.covertEntityToModel(mockProductInfo)

        assertEquals("123", result.productId)
        assertEquals("Test Product", result.title)
        assertEquals("https:/images/test.jpg", result.imageUrl)
        assertEquals(result.maxPrice, "20.0")
        assertEquals(result.minPrice, "10.0")
        assertEquals(listOf("Message 1", "Message 2"), result.messages)
    }

    @Test
    fun `getProductMessage should map ProductMessage list to List of Strings`() {

        val mockProductMessageList = listOf(
            ProductMessage(productId = "123", title = "Message 1"),
            ProductMessage(productId = "123", title = "Message 2")
        )


        val result = mapper.getProductMessage(mockProductMessageList)

        assertEquals(listOf("Message 1", "Message 2"), result)
    }
}
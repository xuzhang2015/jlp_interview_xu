package com.example.domain.repository

import com.example.data.datasource.INetworkDataSource
import com.example.data.model.DummyData
import com.example.data.model.DummyData.dummyDataProdcutId
import com.example.data.model.productDetail.ProductItemDetail
import com.example.domain.CoroutineTestRule
import com.example.domain.mapper.IMapper
import com.example.domain.model.DummyModel
import com.example.domain.model.ProductDetailModel
import kotlinx.coroutines.flow.flowOf
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnit
import org.mockito.kotlin.any

class GetProductDetailRepositoryTest {
    @get:Rule
    val mockitoRule = MockitoJUnit.rule()

    @get:Rule
    val coroutineRule = CoroutineTestRule()

    @Mock
    lateinit var networkDataSource: INetworkDataSource

    @Mock
    lateinit var mapper: IMapper<ProductItemDetail, ProductDetailModel>

    private lateinit var respository: GetProductDetailRepository

    @Before
    fun setup() {
        respository = GetProductDetailRepository(networkDataSource, mapper)
    }

    @Test
    fun `getData test`() = coroutineRule.runBlockingTest {

        Mockito.`when`(
            networkDataSource.requestProductDetail(dummyDataProdcutId)
        ).thenReturn(
            flowOf(DummyData.dummyProductDetail)
        )

        Mockito.`when`(
            mapper.covertEntityToModel(DummyData.dummyProductDetail)
        ).thenReturn(
            DummyModel.dummyProductDetailModel
        )

        val result = respository.getData(dummyDataProdcutId)
        result.collect { }

        Mockito.verify(networkDataSource).requestProductDetail(any())
        Mockito.verifyNoMoreInteractions(networkDataSource)
        Mockito.verify(mapper, Mockito.times(1))
            .covertEntityToModel(DummyData.dummyProductDetail)
        Mockito.verifyNoMoreInteractions(mapper)
    }
}
package com.example.domain.repository

import com.example.data.datasource.INetworkDataSource
import com.example.data.model.DummyData.dummyProductInfo
import com.example.data.model.DummyData.dummyProductInfoList
import com.example.data.model.productList.ProductInfo
import com.example.domain.CoroutineTestRule
import com.example.domain.mapper.IMapper
import com.example.domain.model.DummyModel.dummyProductInfoModel
import com.example.domain.model.ProductInfoModel
import kotlinx.coroutines.flow.flowOf
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito.times
import org.mockito.Mockito.verify
import org.mockito.Mockito.verifyNoMoreInteractions
import org.mockito.Mockito.`when`
import org.mockito.junit.MockitoJUnit

class GetProductListRepositoryTest {
    @get:Rule
    val mockitoRule = MockitoJUnit.rule()

    @get:Rule
    val coroutineRule = CoroutineTestRule()

    @Mock
    lateinit var networkDataSource: INetworkDataSource

    @Mock
    lateinit var mapper: IMapper<ProductInfo, ProductInfoModel>

    private lateinit var respository: GetProductListRepository

    @Before
    fun setup() {
        respository = GetProductListRepository(networkDataSource, mapper)
    }

    @Test
    fun `getData test`() = coroutineRule.runBlockingTest {

        `when`(
            networkDataSource.requestSearchProductList()
        ).thenReturn(
            flowOf(dummyProductInfoList)
        )

        `when`(
            mapper.covertEntityToModel(dummyProductInfo)
        ).thenReturn(
            dummyProductInfoModel
        )

        val result = respository.getData(Unit)
        val resultList = mutableListOf<ProductInfoModel>()
        result.collect { resultList.addAll(it) }

        verify(networkDataSource).requestSearchProductList()
        verifyNoMoreInteractions(networkDataSource)
        verify(mapper, times(dummyProductInfoList.size)).covertEntityToModel(dummyProductInfo)
        verifyNoMoreInteractions(mapper)
    }
}
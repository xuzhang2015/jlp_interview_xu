package com.example.domain.mapper

import com.example.data.model.DummyData.dummyProductDetail
import com.example.data.model.DummyData.dummyProductDetails
import com.example.data.model.DummyData.dummyProductMedia
import com.example.domain.model.DummyModel.dummyProductDetailModel
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test

class ProductDetailMapperTest {

    private lateinit var mapper: ProductDetailMapper

    @Before
    fun setup() {
        mapper = ProductDetailMapper()
    }

    @Test
    fun `covertEntityToModel should map ProductInfo to ProductInfoModel`() {

        val result = mapper.covertEntityToModel(dummyProductDetail)

        assertEquals(dummyProductDetailModel.title, result.title)
        assertEquals(dummyProductDetailModel.imageUrls, result.imageUrls)
        assertEquals(dummyProductDetailModel.productInformation, result.productInformation)
        assertEquals(dummyProductDetailModel.features, result.features)
    }

    @Test
    fun `getImageUrs should map ProductMedia images to List of Strings`() {
        val result = mapper.getImageUrs(dummyProductMedia)

        assertEquals(dummyProductDetailModel.imageUrls, result)
    }

    @Test
    fun `filterNecessaryFeatures should filter and map ProductDetails attributes`() {
        val result = mapper.filterNecessaryFeatures(dummyProductDetails)

        assertEquals(dummyProductDetailModel.features, result)
    }
}
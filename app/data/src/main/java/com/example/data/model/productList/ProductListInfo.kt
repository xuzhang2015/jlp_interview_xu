package com.example.data.model.productList

data class ProductListInfo(
    val products: List<ProductInfo>
)

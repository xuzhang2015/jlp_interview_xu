package com.example.data.model

import com.example.data.model.productDetail.ProductAttribute
import com.example.data.model.productDetail.ProductDetails
import com.example.data.model.productDetail.ProductFeature
import com.example.data.model.productDetail.ProductImages
import com.example.data.model.productDetail.ProductItemDetail
import com.example.data.model.productDetail.ProductMedia
import com.example.data.model.productDetail.ProductPrice
import com.example.data.model.productList.ProductInfo
import com.example.data.model.productList.ProductListInfo
import com.example.data.model.productList.ProductMessage
import com.example.data.model.productList.ProductPriceDisplay
import com.example.data.model.productList.ProductPriceValue
import com.example.data.model.productList.VariantPriceRange

object DummyData {
    val dummyDataProdcutId = "110488704"

    val dummyProductPriceDisplay = ProductPriceDisplay(
        min = "£549.00",
        max = "549.00",
    )

    val dummyProductPriceValue = ProductPriceValue(
        max = 400F,
        min = 400F,
    )

    val dummyVariantPriceRange = VariantPriceRange(
        display = dummyProductPriceDisplay,
        value = dummyProductPriceValue,
    )

    val dummyProductMessage = ProductMessage(
        productId = dummyDataProdcutId,
        title = "Claim a free extra 3 year guarantee (Via redemption)"
    )

    val dummyProductInfo = ProductInfo(
        productId = dummyDataProdcutId,
        title = "Bosch Series 4 SMS4HMW00G Freestanding Dishwasher, White",
        image = "//johnlewis.scene7.com/is/image/JohnLewis/234378764?",
        variantPriceRange = dummyVariantPriceRange,
        messaging = listOf(dummyProductMessage),
    )

    val dummyProductInfoList = listOf(
        dummyProductInfo,
        dummyProductInfo,
        dummyProductInfo,
    )

    val dummyProductListInfo = ProductListInfo(
        dummyProductInfoList
    )

    val productImages = ProductImages(
        urls = listOf(
            "//johnlewis.scene7.com/is/image/JohnLewis/110429243?",
            "//johnlewis.scene7.com/is/image/JohnLewis/110429243alt1?",
            "//johnlewis.scene7.com/is/image/JohnLewis/110429243alt2?",
            "//johnlewis.scene7.com/is/image/JohnLewis/110429243alt3?",
            "//johnlewis.scene7.com/is/image/JohnLewis/110429243alt4?",
            "//johnlewis.scene7.com/is/image/JohnLewis/110429243alt5?",
            "//johnlewis.scene7.com/is/image/JohnLewis/110429243alt6?",
            "//johnlewis.scene7.com/is/image/JohnLewis/110429243alt10?",

            )
    )

    val dummyProductMedia = ProductMedia(
        images = productImages
    )

    val dummyProductPrice = ProductPrice(
        was = "",
        then1 = "",
        then2 = "",
        now = "",
        uom = "",
        currency = "",
    )

    val dummyAttribute1 = ProductAttribute(
        name = "Guarantee",
        value = "2 year guarantee included",
        multivalued = false,
    )
    val dummyAttribute2 = ProductAttribute(
        name = "Adjustable",
        value = "Yes",
        multivalued = false,
    )
    val dummyAttribute3 = ProductAttribute(
        name = "Child lock",
        value = "No",
        multivalued = false,
    )
    val dummyAttribute4 = ProductAttribute(
        name = "Delay Start",
        value = "Yes - up to 24 hours",
        multivalued = false,
    )
    val dummyAttribute5 = ProductAttribute(
        name = "Delicate Wash",
        value = "Yes",
        multivalued = false,
    )
    val dummyAttribute6 = ProductAttribute(
        name = "Dimensions",
        value = "H84.5 x W60 x D60cm",
        multivalued = false,
    )
    val dummyAttribute7 = ProductAttribute(
        name = "Drying performance",
        value = "B",
        multivalued = false,
    )

    val dummyAttribute8 = ProductAttribute(
        name = "Drying system",
        value = "Heat Exchanger",
        multivalued = false,
    )

    val dummyAttributes = listOf(
        dummyAttribute1,
        dummyAttribute2,
        dummyAttribute3,
        dummyAttribute4,
        dummyAttribute5,
        dummyAttribute6,
        dummyAttribute7,
        dummyAttribute8
    )

    val dummyProductFeature = ProductFeature(
        dummyAttributes
    )

    val dummyProductDetails = ProductDetails(
        productInformation = "<p>Forget slaving over the washing up and missing out on family time. Bosch helps keep your worktops free from clutter thanks to the spacious interior and variety of cleaning options they've packed into this dishwasher. The 60 minute quick wash cycle is really handy, especially if you forget the kids are having their friends over for tea, and the adjustable racking means there's space for any larger items that need a clean too. Plus, Heat Exchanger drying gets rid of moisture with warmth from the wash, for cupboard-ready glasses and cutlery every time.</p>",
        features = listOf(dummyProductFeature)
    )


    val dummyProductDetail = ProductItemDetail(
        title = "Bosch Series 4 SMS4HMW00G Freestanding Dishwasher, White",
        code = "81701204",
        price = dummyProductPrice,
        media = dummyProductMedia,
        details = dummyProductDetails,
    )
}
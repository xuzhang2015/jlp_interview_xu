package com.example.data.datasource

import com.example.data.model.productDetail.ProductItemDetail
import com.example.data.model.productList.ProductInfo
import kotlinx.coroutines.flow.Flow

interface INetworkDataSource {
    fun requestSearchProductList(): Flow<List<ProductInfo>>
    fun requestProductDetail(productId: String): Flow<ProductItemDetail>
}
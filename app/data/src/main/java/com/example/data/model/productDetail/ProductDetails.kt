package com.example.data.model.productDetail

data class ProductDetails(
    val productInformation: String,
    val features: List<ProductFeature> = listOf()
)

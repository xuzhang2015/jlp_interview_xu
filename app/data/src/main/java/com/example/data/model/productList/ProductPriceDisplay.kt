package com.example.data.model.productList

data class ProductPriceDisplay(
    val min: String,
    val max: String,
)

package com.example.data.model.productDetail

data class ProductPrice(
    val was: String,
    val then1: String,
    val then2: String,
    val now: String,
    val uom: String,
    val currency: String,
)

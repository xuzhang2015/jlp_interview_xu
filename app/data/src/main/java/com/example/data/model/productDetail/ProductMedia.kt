package com.example.data.model.productDetail

data class ProductMedia(
    val images: ProductImages
)

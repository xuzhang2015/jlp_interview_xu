package com.example.data.model.productDetail

data class ProductImages(
    val urls: List<String> = listOf(),
)
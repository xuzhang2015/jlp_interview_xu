package com.example.data.model.productDetail

data class ProductFeature(
    val attributes: List<ProductAttribute> = listOf()
)

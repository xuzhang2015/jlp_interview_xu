package com.example.data.model.productList

data class VariantPriceRange(
    val display: ProductPriceDisplay,
    val value: ProductPriceValue,
)

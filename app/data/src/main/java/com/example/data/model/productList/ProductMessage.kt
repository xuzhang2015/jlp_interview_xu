package com.example.data.model.productList

data class ProductMessage(
    val productId: String,
    val title: String
)

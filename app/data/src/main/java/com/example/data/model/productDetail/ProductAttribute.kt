package com.example.data.model.productDetail

data class ProductAttribute(
    val name: String,
    val value: String,
    val multivalued: Boolean,
)

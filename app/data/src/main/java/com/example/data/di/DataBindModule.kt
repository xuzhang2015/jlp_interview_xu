package com.example.data.di

import com.example.data.datasource.INetworkDataSource
import com.example.data.datasource.NetworkDataSourceImpl
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
abstract class DataBindModule {
    @Binds
    @Singleton
    abstract fun bindNetworkDataSource(networkDataSourceImpl: NetworkDataSourceImpl): INetworkDataSource
}
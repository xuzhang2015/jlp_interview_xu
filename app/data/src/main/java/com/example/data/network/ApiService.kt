package com.example.data.network

import com.example.data.model.productDetail.ProductItemDetail
import com.example.data.model.productList.ProductListInfo
import com.google.gson.JsonSyntaxException
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ApiService {
    @Throws(JsonSyntaxException::class)
    @GET("search/api/rest/v2/catalog/products/search/keyword")
    suspend fun searchProducts(
        @Query("q") keyword: String,
        @Query("key") apiKey: String
    ): Response<ProductListInfo>

    @Throws(JsonSyntaxException::class)
    @GET("mobile-apps/api/v1/products/{productId}")
    suspend fun getProductDetail(@Path("productId") productId: String): Response<ProductItemDetail>
}
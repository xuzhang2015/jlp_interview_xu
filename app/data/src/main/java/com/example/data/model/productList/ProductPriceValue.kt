package com.example.data.model.productList

data class ProductPriceValue(
    val max: Float,
    val min: Float,
)

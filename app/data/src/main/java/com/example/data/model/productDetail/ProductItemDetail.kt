package com.example.data.model.productDetail

data class ProductItemDetail(
    val title: String,
    val code: String,
    val price: ProductPrice,
    val media: ProductMedia,
    val details: ProductDetails,
)

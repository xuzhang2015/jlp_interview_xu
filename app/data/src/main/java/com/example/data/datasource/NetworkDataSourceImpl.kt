package com.example.data.datasource

import android.content.Context
import android.util.Log
import com.example.data.model.productDetail.ProductItemDetail
import com.example.data.model.productList.ProductInfo
import com.example.data.network.ApiService
import com.example.data.network.Ulitis.SEARCH_KEY
import com.example.data.network.Ulitis.SEARCH_REQUEST
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class NetworkDataSourceImpl @Inject constructor(
    private val apiService: ApiService,
    private val context: Context,
) : INetworkDataSource {

    override fun requestSearchProductList(): Flow<List<ProductInfo>> {
        return flow {
            val response = apiService.searchProducts(SEARCH_REQUEST, SEARCH_KEY)
            if (response.isSuccessful) {
                if (response.body() == null) {
                    throw NullPointerException("Response body is null")
                } else {
                    emit(response.body()!!.products)
                }
            } else {
                // Handle specific network-related exceptions, include the GSON converting failure which is the most common error
                error(response.message())
            }
        }.catch { e ->
            Log.e(
                "NetworkDataSourceImpl",
                "Exception during getProductDetail: ${e.localizedMessage}"
            )
            Log.e("NetworkDataSourceImpl", "Exception message: ${e.message}")
            Log.e("NetworkDataSourceImpl", "Exception class: ${e.javaClass.simpleName}")
            Log.e("NetworkDataSourceImpl", "Exception stack trace: ${e.printStackTrace()}")
            // Handle exceptions during the collection phase
            throw NullPointerException("Error requesting product info list: ${e.localizedMessage}")
        }
    }

    override fun requestProductDetail(productId: String): Flow<ProductItemDetail> {
        return flow {
            val response = apiService.getProductDetail(productId)
            if (response.isSuccessful) {
                if (response.body() == null) {
                    throw NullPointerException("Response body is null")
                } else {
                    emit(response.body()!!)
                }
            } else {
                // Handle specific network-related exceptions, include the GSON converting failure which is the most common error
                error(response.message())
            }
        }.catch { e ->
            Log.e(
                "NetworkDataSourceImpl",
                "Exception during getProductDetail: ${e.localizedMessage}"
            )
            Log.e("NetworkDataSourceImpl", "Exception message: ${e.message}")
            Log.e("NetworkDataSourceImpl", "Exception class: ${e.javaClass.simpleName}")
            Log.e("NetworkDataSourceImpl", "Exception stack trace: ${e.printStackTrace()}")
            // Handle exceptions during the collection phase
            throw NullPointerException("Error requesting product detail: ${e.localizedMessage}")
        }
    }
}
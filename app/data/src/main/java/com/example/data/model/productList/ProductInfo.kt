package com.example.data.model.productList

data class ProductInfo(
    val productId: String,
    val title: String,
    val image: String,
    val variantPriceRange: VariantPriceRange,
    val messaging: List<ProductMessage>,
)

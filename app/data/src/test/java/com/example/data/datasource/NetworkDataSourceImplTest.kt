package com.example.data.datasource

import android.content.Context
import com.example.data.CoroutineTestRule
import com.example.data.model.DummyData.dummyDataProdcutId
import com.example.data.model.DummyData.dummyProductDetail
import com.example.data.model.DummyData.dummyProductListInfo
import com.example.data.network.ApiService
import com.example.data.network.Ulitis.SEARCH_KEY
import com.example.data.network.Ulitis.SEARCH_REQUEST
import junit.framework.TestCase.assertEquals
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.toList
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.junit.MockitoJUnit
import retrofit2.Response

@OptIn(ExperimentalCoroutinesApi::class)
class NetworkDataSourceImplTest {

    @get:Rule
    val mockitoRule = MockitoJUnit.rule()

    @get:Rule
    val coroutineRule = CoroutineTestRule()

    @Mock
    private lateinit var apiService: ApiService

    @Mock
    private lateinit var context: Context

    private lateinit var networkDataSource: NetworkDataSourceImpl

    @Before
    fun setup() {
        networkDataSource = NetworkDataSourceImpl(apiService, context)
    }

    @Test
    fun `requestSearchProductList should return a list of ProductInfo`() =
        coroutineRule.runBlockingTest {
            val mockResponse = Response.success(dummyProductListInfo)

            `when`(apiService.searchProducts(SEARCH_REQUEST, SEARCH_KEY)).thenReturn(mockResponse)

            val result = networkDataSource.requestSearchProductList().toList()

            assertEquals(1, result.size)
            assertEquals(dummyProductListInfo.products, result[0])
        }

    @Test
    fun `requestProductDetail should return a ProductItemDetail`() = runBlockingTest {
        val mockResponse = Response.success(dummyProductDetail)

        `when`(apiService.getProductDetail(dummyDataProdcutId)).thenReturn(mockResponse)

        val result = networkDataSource.requestProductDetail(dummyDataProdcutId).toList()

        assertEquals(1, result.size)
        assertEquals(dummyProductDetail, result[0])
    }
}
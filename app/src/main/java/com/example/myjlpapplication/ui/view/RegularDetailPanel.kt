package com.example.myjlpapplication.ui.view

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.KeyboardArrowRight
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.example.domain.model.DummyModel.dummyProductDetailModel
import com.example.domain.model.ProductDetailModel
import com.example.myjlpapplication.R

@Preview
@Composable
fun PreviewRegularDetailPanel() {
    RegularDetailPanel(item = dummyProductDetailModel)
}

@Composable
fun RegularDetailPanel(
    modifier: Modifier = Modifier,
    item: ProductDetailModel
) {
    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(16.dp)
    ) {

        Row(
            modifier = Modifier.fillMaxSize()
        ) {
            Box(
                modifier = Modifier
                    .weight(2F)
                    .fillMaxSize()
            ) {
                LeftColumnPanel(item)
            }

            Spacer(
                modifier = Modifier
                    .fillMaxHeight()
                    .background(Color.Gray)
                    .width(1.dp)
            )

            Box(
                modifier = Modifier
                    .weight(1F)
                    .fillMaxSize()
                    .padding(16.dp)
            ) {
                RightColumnPanel(item)
            }
        }
    }
}

@Composable
fun LeftColumnPanel(item: ProductDetailModel) {
    Column(
        modifier = Modifier
            .verticalScroll(rememberScrollState())
            .fillMaxSize()
    ) {
        Box(
            modifier = Modifier
                .fillMaxWidth()
                .height(450.dp)
        ) {
            ImageCarousel(imageUrs = item.imageUrls)
        }
        Spacer(
            modifier = Modifier
                .fillMaxWidth()
                .background(Color.White)
                .height(10.dp)
        )

        Box(
            modifier = Modifier
                .fillMaxWidth()
                .height(300.dp)
        ) {
            RegularInformationPanel(item = item)
        }

        Spacer(
            modifier = Modifier
                .fillMaxWidth()
                .background(Color.Gray)
                .height(1.dp)
        )

        Box(
            modifier = Modifier
                .fillMaxWidth()
                .height(300.dp)
        ) {
            ProductSpecialPanel(features = item.features)
        }

        Spacer(
            modifier = Modifier
                .fillMaxWidth()
                .background(Color.White)
                .height(60.dp)
        )
    }
}

@Preview
@Composable
fun PreviewRegularInformationPanel() {
    RegularInformationPanel(item = dummyProductDetailModel)
}

@Composable
fun RegularInformationPanel(item: ProductDetailModel) {
    Column(
        modifier = Modifier
            .fillMaxSize()
            .background(Color.White),
        verticalArrangement = Arrangement.SpaceEvenly,
    ) {
        Text(
            modifier = Modifier
                .fillMaxWidth()
                .weight(1F)
                .height(30.dp),
            text = stringResource(R.string.product_information),
            color = Color.Gray,
            style = MaterialTheme.typography.headlineMedium,
            fontWeight = FontWeight.Normal,
            textAlign = TextAlign.Start
        )
        Spacer(
            modifier = Modifier
                .fillMaxWidth()
                .background(Color.White)
                .height(20.dp)
        )
        Text(
            modifier = Modifier
                .fillMaxWidth()
                .weight(1F)
                .height(30.dp),
            text = "Product code: " + item.code,
            color = Color.Black,
            style = MaterialTheme.typography.bodyMedium,
            fontWeight = FontWeight.Normal,
            textAlign = TextAlign.Start
        )

        Spacer(
            modifier = Modifier
                .fillMaxWidth()
                .background(Color.White)
                .height(5.dp)
        )
        Box(
            modifier = Modifier
                .fillMaxWidth()
                .weight(2F)
        ) {
            DisplayHtmlContentPanel(htmlText = item.productInformation)
        }

        Spacer(
            modifier = Modifier
                .fillMaxWidth()
                .background(Color.Gray)
                .height(1.dp)
        )

        Row(
            modifier = Modifier
                .fillMaxWidth()
                .height(40.dp),
            horizontalArrangement = Arrangement.SpaceBetween,
            verticalAlignment = Alignment.CenterVertically
        ) {
            Text(
                modifier = Modifier
                    .fillMaxSize()
                    .weight(1F)
                    .wrapContentHeight(Alignment.CenterVertically),
                maxLines = 1,
                overflow = TextOverflow.Ellipsis,
                text = "Read more",
                color = Color.Black,
                style = MaterialTheme.typography.bodyLarge,
                fontWeight = FontWeight.Bold,
                textAlign = TextAlign.Start
            )

            IconButton(
                modifier = Modifier.size(50.dp),
                onClick = { }
            ) {
                Icon(imageVector = Icons.Default.KeyboardArrowRight, contentDescription = "Back")
            }
        }
        Spacer(
            modifier = Modifier
                .fillMaxWidth()
                .background(Color.Gray)
                .height(1.dp)
        )

    }
}

@Composable
fun RightColumnPanel(item: ProductDetailModel) {
    PriceAndMessaging(item = item)
}
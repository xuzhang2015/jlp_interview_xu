package com.example.myjlpapplication.ui.navigation

object Route {
    const val HOME = "home"
    const val DETAIL = "detail/{productId}"
    const val PRODUCT_ID = "productId"

    fun getProductDetailScreenRoute(productId: String) = "detail/$productId"
}
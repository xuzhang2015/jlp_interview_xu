package com.example.myjlpapplication.ui.view

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.KeyboardArrowLeft
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import com.example.domain.model.DummyModel.dummyProductDetailModel
import com.example.domain.model.ProductDetailModel
import com.example.myjlpapplication.Ulitis.THRESHOLD_TABLET_AND_MOBILE
import com.example.myjlpapplication.ui.state.UIState
import com.example.myjlpapplication.ui.viewmodel.ProductDetailViewModel


@Composable
fun ProductDetailScreen(
    viewModel: ProductDetailViewModel = hiltViewModel(),
    modifier: Modifier = Modifier,
    value: String,
    backButtonClick: () -> Unit,
) {
    LaunchedEffect(value) {
        viewModel.getProductDetail(value)
    }

    val state by viewModel.uiState.collectAsState()

    when (state) {
        is UIState.ErrorAppear -> {
            ErrorScreen(message = (state as UIState.ErrorAppear).errorMessage)
        }

        UIState.Loading -> {
            LoadingScreen()
        }

        is UIState.ProductDetail -> {
            LoadingScreen()
            (state as? UIState.ProductDetail)?.let { data ->
                DetailPanel(item = data.item, backButtonClick = backButtonClick)
            }
        }

        else -> {
        }
    }
}

@Preview
@Composable
fun PreviewDetailPanel() {
    DetailPanel(item = dummyProductDetailModel) {

    }
}

@Composable
fun DetailPanel(
    modifier: Modifier = Modifier,
    item: ProductDetailModel,
    backButtonClick: () -> Unit,
) {
    val isTablet = LocalConfiguration.current.smallestScreenWidthDp >= THRESHOLD_TABLET_AND_MOBILE

    Box(modifier = Modifier.background(Color.Gray)) {
        Column(
            modifier = Modifier
                .padding(10.dp)
                .background(Color.White)
                .fillMaxSize(),
            verticalArrangement = Arrangement.Center,
        ) {
            Row(
            ) {
                CustomBackButton(backButtonClick = backButtonClick)
                Text(
                    modifier = Modifier
                        .fillMaxWidth()
                        .height(50.dp),
                    text = item.title,
                    color = Color.Gray,
                    style = MaterialTheme.typography.bodyMedium,
                    fontWeight = FontWeight.Normal,
                    textAlign = TextAlign.Center
                )
            }
            Spacer(
                modifier = Modifier
                    .fillMaxWidth()
                    .height(1.dp)
                    .background(Color.Gray)
            )
            if (isTablet) {
                RegularDetailPanel(item = item)
            } else {
                CompactDetailPanel(item = item)
            }
        }
    }
}

@Composable
fun CustomBackButton(
    modifier: Modifier = Modifier,
    backButtonClick: () -> Unit
) {
    IconButton(
        modifier = Modifier.size(50.dp),
        onClick = { backButtonClick.invoke() }
    ) {
        Icon(imageVector = Icons.Default.KeyboardArrowLeft, contentDescription = "Back")
    }
}
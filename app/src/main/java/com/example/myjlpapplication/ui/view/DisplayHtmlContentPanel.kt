package com.example.myjlpapplication.ui.view

import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.tooling.preview.Preview
import androidx.core.text.HtmlCompat


@Preview
@Composable
fun PreviewDisplayHtmlContentPanel() {
    DisplayHtmlContentPanel(htmlText = "<p>This is a <b>bold</b> and <i>italic</i> text.</p>")
}

@Composable
fun DisplayHtmlContentPanel(
    modifier: Modifier = Modifier,
    htmlText: String
) {
    val annotatedString = buildAnnotatedString {
        append(HtmlCompat.fromHtml(htmlText, HtmlCompat.FROM_HTML_MODE_LEGACY))
    }

    Text(
        text = annotatedString,
        modifier = Modifier
            .fillMaxSize()
    )
}
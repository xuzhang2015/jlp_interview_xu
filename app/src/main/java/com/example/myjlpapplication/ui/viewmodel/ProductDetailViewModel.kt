package com.example.myjlpapplication.ui.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.domain.model.ProductDetailModel
import com.example.domain.usercase.IUserCase
import com.example.myjlpapplication.ui.state.UIState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ProductDetailViewModel @Inject constructor(
    private val productDetailUserCase: IUserCase<String, ProductDetailModel>,
    private val sharedDataSingleton: SharedDataSingleton,
) : ViewModel() {

    private val _uiState = MutableStateFlow<UIState<ProductDetailModel>>(UIState.Loading)
    val uiState: StateFlow<UIState<ProductDetailModel>> get() = _uiState

    fun getProductDetail(productId: String) {
        _uiState.value = UIState.Loading
        viewModelScope.launch {
            productDetailUserCase.execute(productId).catch {
                _uiState.value = UIState.ErrorAppear(it.message.toString())
            }.collect { data ->
                val newData = data[0].copy(
                    price = sharedDataSingleton.getSharedData().price,
                    messages = sharedDataSingleton.getSharedData().messages
                )
                _uiState.value = UIState.ProductDetail(newData)
            }
        }
    }
}
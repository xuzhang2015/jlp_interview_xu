package com.example.myjlpapplication.ui.view

import android.content.res.Configuration
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.foundation.lazy.grid.LazyVerticalGrid
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import com.example.domain.model.DummyModel.dummyProductInfoModelList
import com.example.domain.model.ProductInfoModel
import com.example.domain.model.SharedDataModel
import com.example.myjlpapplication.R
import com.example.myjlpapplication.Ulitis.LANDSCAPE_COLUMN
import com.example.myjlpapplication.Ulitis.NORMAL_COLUMN
import com.example.myjlpapplication.ui.state.UIState
import com.example.myjlpapplication.ui.viewmodel.ProductListViewModel

@Composable
fun ProductHomeScreen(
    viewModel: ProductListViewModel = hiltViewModel(),
    modifier: Modifier = Modifier,
    itemClick: (String) -> Unit,
) {
    LaunchedEffect(Unit) {
        viewModel.getProductInfoData()
    }

    val state by viewModel.uiState.collectAsState()

    when (state) {
        is UIState.ErrorAppear -> {
            ErrorScreen(message = (state as UIState.ErrorAppear).errorMessage)
        }

        UIState.Loading -> {
            LoadingScreen()
        }

        is UIState.ProductList -> {
            LoadingScreen()
            (state as? UIState.ProductList)?.let { data ->
                HomePanel(itemList = data.list) {
                    viewModel.updateShareData(it)
                    itemClick.invoke(it.productId)
                }
            }
        }

        else -> {}
    }
}

@Preview
@Composable
fun PreviewHomePanel() {
    HomePanel(itemList = dummyProductInfoModelList) {

    }
}

@Composable
fun HomePanel(
    modifier: Modifier = Modifier,
    itemList: List<ProductInfoModel>,
    itemClick: (SharedDataModel) -> Unit,
) {
    val isLandscape = LocalConfiguration.current.orientation == Configuration.ORIENTATION_LANDSCAPE

    val itemsPerRow = when {
        isLandscape -> LANDSCAPE_COLUMN
        else -> NORMAL_COLUMN
    }

    Box(modifier = Modifier.background(Color.Gray)) {
        Column(
            modifier = Modifier
                .padding(10.dp)
                .fillMaxSize(),
            verticalArrangement = Arrangement.Center,
        ) {

            Text(
                modifier = Modifier
                    .fillMaxWidth()
                    .height(50.dp)
                    .background(Color.White),
                text = stringResource(R.string.dishwashers, itemList.size),
                color = Color.Gray,
                style = MaterialTheme.typography.headlineLarge,
                fontWeight = FontWeight.Normal,
                textAlign = TextAlign.Center
            )
            GridDistributionPanel(itemsPerRow, itemList, itemClick)

        }
    }
}

@Preview
@Composable
fun PreviewLandscapeGridPanel() {
    GridDistributionPanel(itemsPerRow = 4, itemList = dummyProductInfoModelList) {

    }
}

@Composable
fun GridDistributionPanel(
    itemsPerRow: Int,
    itemList: List<ProductInfoModel>,
    itemClick: (SharedDataModel) -> Unit
) {
    LazyVerticalGrid(
        columns = GridCells.Fixed(itemsPerRow), // 指定每行显示的列数
        contentPadding = PaddingValues(2.dp),
        verticalArrangement = Arrangement.spacedBy(1.dp), // 设置垂直方向上的间隔

        modifier = Modifier
            .fillMaxSize()
            .background(Color.White)
    ) {
        items(itemList.size) { index ->
            GridItemPanel(itemList[index], itemClick)
        }
    }
}
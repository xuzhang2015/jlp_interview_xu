package com.example.myjlpapplication.ui.state

sealed class UIState<out T> {
    object Loading : UIState<Nothing>()
    data class ErrorAppear(val errorMessage: String) : UIState<Nothing>()
    data class ProductList<T>(val list: List<T>) : UIState<T>()
    data class ProductDetail<T>(val item: T) : UIState<T>()
}

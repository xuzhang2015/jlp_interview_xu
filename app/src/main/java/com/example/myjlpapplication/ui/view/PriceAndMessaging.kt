package com.example.myjlpapplication.ui.view

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.example.domain.Ulitis.GUAEANTEE
import com.example.domain.model.DummyModel.dummyProductDetailModel
import com.example.domain.model.ProductDetailModel

@Preview
@Composable
fun PreviewPriceAndMessaging() {
    PriceAndMessaging(item = dummyProductDetailModel)
}

@Composable
fun PriceAndMessaging(
    modifier: Modifier = Modifier,
    item: ProductDetailModel
) {
    Column(
        modifier = Modifier
            .fillMaxSize()
            .background(Color.White)
    ) {
        Text(
            modifier = Modifier
                .fillMaxWidth()
                .height(40.dp),
            text = item.price,
            color = Color.Black,
            style = MaterialTheme.typography.headlineLarge,
            fontWeight = FontWeight.Normal,
            textAlign = TextAlign.Start
        )

        Spacer(
            modifier = Modifier
                .fillMaxWidth()
                .height(5.dp)
        )

        Text(
            modifier = Modifier
                .fillMaxWidth()
                .height(50.dp),
            text = item.messages.getOrNull(0) ?: "",
            color = Color.Black,
            style = MaterialTheme.typography.bodyMedium,
            fontWeight = FontWeight.Normal,
            textAlign = TextAlign.Start
        )

        Spacer(
            modifier = Modifier
                .fillMaxWidth()
                .height(5.dp)
        )

        Text(
            modifier = Modifier
                .fillMaxWidth()
                .height(30.dp),
            text = item.features[GUAEANTEE] ?: "",
            color = Color.Gray,
            style = MaterialTheme.typography.bodyMedium,
            fontWeight = FontWeight.Normal,
            textAlign = TextAlign.Start
        )
    }

}
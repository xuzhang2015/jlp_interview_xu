package com.example.myjlpapplication.ui.viewmodel

import com.example.domain.model.SharedDataModel
import javax.inject.Inject


open class SharedDataSingleton @Inject constructor() {
    private var sharedData: SharedDataModel = SharedDataModel()

    open fun getSharedData(): SharedDataModel {
        return sharedData
    }

    open fun setSharedData(data: SharedDataModel) {
        sharedData = data
    }
}
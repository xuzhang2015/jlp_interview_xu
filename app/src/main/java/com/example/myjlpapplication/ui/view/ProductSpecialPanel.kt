package com.example.myjlpapplication.ui.view

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.example.domain.Ulitis.ADJUSTABLE_RACKING_INFORMATION_DISPLAY
import com.example.domain.Ulitis.CHILD_LOCK_DOOR_DISPLAY
import com.example.domain.Ulitis.DELAY_WASH_DISPLAY
import com.example.domain.Ulitis.DELICATE_WASH_DISPLAY
import com.example.domain.Ulitis.DIMENSIONS_DISPLAY
import com.example.domain.Ulitis.DRYING_PERFORMANCE_DISPLAY
import com.example.domain.Ulitis.DRYING_SYSTEM_DISPLAY
import com.example.domain.model.DummyModel.dummyProductFeatures

@Preview
@Composable
fun PreviewProductSpecialPanel() {
    ProductSpecialPanel(features = dummyProductFeatures)
}

@Composable
fun ProductSpecialPanel(
    modifier: Modifier = Modifier,
    features: Map<String, String>
) {
    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(0.dp, 0.dp, 16.dp, 0.dp),
    ) {
        Text(
            modifier = Modifier
                .fillMaxWidth()
                .height(60.dp)
                .wrapContentHeight(Alignment.CenterVertically),
            text = "Product Specification",
            color = Color.Gray,
            style = MaterialTheme.typography.headlineLarge,
            fontWeight = FontWeight.Normal,
            textAlign = TextAlign.Start
        )

        Spacer(
            modifier = Modifier
                .fillMaxWidth()
                .height(1.dp)
                .background(Color.Gray)
        )

        ProductSpecialItemPanel(
            name = ADJUSTABLE_RACKING_INFORMATION_DISPLAY,
            value = features[ADJUSTABLE_RACKING_INFORMATION_DISPLAY] ?: ""
        )

        Spacer(
            modifier = Modifier
                .fillMaxWidth()
                .height(1.dp)
                .background(Color.Gray)
        )

        ProductSpecialItemPanel(
            name = CHILD_LOCK_DOOR_DISPLAY,
            value = features[CHILD_LOCK_DOOR_DISPLAY] ?: ""
        )

        Spacer(
            modifier = Modifier
                .fillMaxWidth()
                .height(1.dp)
                .background(Color.Gray)
        )

        ProductSpecialItemPanel(
            name = DELAY_WASH_DISPLAY,
            value = features[DELAY_WASH_DISPLAY] ?: ""
        )

        Spacer(
            modifier = Modifier
                .fillMaxWidth()
                .height(1.dp)
                .background(Color.Gray)
        )

        ProductSpecialItemPanel(
            name = DELICATE_WASH_DISPLAY,
            value = features[DELICATE_WASH_DISPLAY] ?: ""
        )

        Spacer(
            modifier = Modifier
                .fillMaxWidth()
                .height(1.dp)
                .background(Color.Gray)
        )

        ProductSpecialItemPanel(
            name = DIMENSIONS_DISPLAY,
            value = features[DIMENSIONS_DISPLAY] ?: ""
        )

        Spacer(
            modifier = Modifier
                .fillMaxWidth()
                .height(1.dp)
                .background(Color.Gray)
        )

        ProductSpecialItemPanel(
            name = DRYING_PERFORMANCE_DISPLAY,
            value = features[DRYING_PERFORMANCE_DISPLAY] ?: ""
        )

        Spacer(
            modifier = Modifier
                .fillMaxWidth()
                .height(1.dp)
                .background(Color.Gray)
        )

        ProductSpecialItemPanel(
            name = DRYING_SYSTEM_DISPLAY,
            value = features[DRYING_SYSTEM_DISPLAY] ?: ""
        )
    }
}
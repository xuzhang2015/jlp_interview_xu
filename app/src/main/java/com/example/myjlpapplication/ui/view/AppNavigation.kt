package com.example.myjlpapplication.ui.view

import androidx.activity.compose.BackHandler
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgument
import com.example.myjlpapplication.ui.navigation.Route.DETAIL
import com.example.myjlpapplication.ui.navigation.Route.HOME
import com.example.myjlpapplication.ui.navigation.Route.PRODUCT_ID
import com.example.myjlpapplication.ui.navigation.Route.getProductDetailScreenRoute

@Composable
fun AppNavigation(modifier: Modifier = Modifier, onExistApp: () -> Unit) {
    val navigationController = rememberNavController()

    BackHandler {
        if (navigationController.previousBackStackEntry != null) {
            navigationController.popBackStack()
        } else {
            onExistApp.invoke()
        }
    }

    NavHost(navController = navigationController, startDestination = HOME) {
        composable(HOME) {
            ProductHomeScreen() {
                navigationController.navigate(getProductDetailScreenRoute(it))
            }
        }
        composable(DETAIL, arguments = listOf(
            navArgument(PRODUCT_ID) {
                this.type = NavType.StringType
            }
        )) {
            val value = it.arguments?.getString(PRODUCT_ID) ?: ""
            ProductDetailScreen(value = value) {
                navigationController.navigateUp()
            }
        }
    }
}
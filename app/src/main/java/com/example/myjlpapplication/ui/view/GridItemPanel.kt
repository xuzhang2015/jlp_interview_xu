package com.example.myjlpapplication.ui.view

import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.bumptech.glide.integration.compose.ExperimentalGlideComposeApi
import com.bumptech.glide.integration.compose.GlideImage
import com.example.domain.model.DummyModel.dummyProductInfoModel
import com.example.domain.model.ProductInfoModel
import com.example.domain.model.SharedDataModel

@Preview
@Composable
fun PreviewGridItemPanel() {
    GridItemPanel(item = dummyProductInfoModel) {
    }
}

@OptIn(ExperimentalGlideComposeApi::class)
@Composable
fun GridItemPanel(item: ProductInfoModel, itemClick: (SharedDataModel) -> Unit) {
    Box(
        modifier = Modifier
            .fillMaxSize()
            .border(1.dp, MaterialTheme.colorScheme.primary)
            .clickable {
                itemClick(
                    SharedDataModel(
                        productId = item.productId,
                        price = item.maxPrice,
                        messages = item.messages,
                    )
                )
            }
    ) {
        Column(
            modifier = Modifier
                .fillMaxSize()
                .padding(10.dp),
            verticalArrangement = Arrangement.Center,
        ) {
            Box(
                modifier = Modifier
                    .fillMaxSize(),
                contentAlignment = Alignment.Center,
            ) {
                GlideImage(
                    modifier = Modifier
                        .fillMaxSize()
                        .padding(10.dp),
                    model = item.imageUrl,
                    contentDescription = ""
                )
            }
            Column(
                modifier = Modifier
                    .fillMaxSize()
                    .padding(10.dp),
                verticalArrangement = Arrangement.Center
            ) {

                Text(
                    modifier = Modifier.height(90.dp),
                    maxLines = 3,
                    overflow = TextOverflow.Ellipsis,
                    text = item.title,
                    style = MaterialTheme.typography.bodyLarge.copy(fontWeight = FontWeight.Normal),
                )
                Text(
                    modifier = Modifier.height(20.dp),
                    text = item.maxPrice,
                    style = MaterialTheme.typography.bodyLarge.copy(fontWeight = FontWeight.Bold),
                )
            }
        }

    }
}
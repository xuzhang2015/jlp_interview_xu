package com.example.myjlpapplication.ui.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.domain.model.ProductInfoModel
import com.example.domain.model.SharedDataModel
import com.example.domain.usercase.IUserCase
import com.example.myjlpapplication.ui.state.UIState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ProductListViewModel @Inject constructor(
    private val productInfoListUserCase: IUserCase<Unit, ProductInfoModel>,
    private val sharedDataSingleton: SharedDataSingleton,
) : ViewModel() {


    private val _uiState = MutableStateFlow<UIState<ProductInfoModel>>(UIState.Loading)
    val uiState: StateFlow<UIState<ProductInfoModel>> get() = _uiState

    fun getProductInfoData() {
        viewModelScope.launch {
            productInfoListUserCase.execute(Unit).flowOn(Dispatchers.IO).catch {
                _uiState.value = UIState.ErrorAppear(it.message.toString())
            }.collect { data ->
                _uiState.value = UIState.ProductList(data)
            }
        }
    }

    fun updateShareData(shareData: SharedDataModel) {
        sharedDataSingleton.setSharedData(shareData)
    }
}
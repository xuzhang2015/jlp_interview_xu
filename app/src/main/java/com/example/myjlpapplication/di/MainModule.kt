package com.example.myjlpapplication.di

import com.example.myjlpapplication.ui.viewmodel.SharedDataSingleton
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class MainModule {
    @Singleton
    @Provides
    fun provideSharedDataSingleton(): SharedDataSingleton {
        return SharedDataSingleton()
    }
}
package com.example.myjlpapplication.ui.viewmodel

import com.example.CoroutineTestRule
import com.example.domain.model.DummyModel
import com.example.domain.model.DummyModel.dummyProductId
import com.example.domain.model.DummyModel.dummySharedDataModel
import com.example.domain.model.ProductDetailModel
import com.example.domain.usercase.IUserCase
import com.example.myjlpapplication.ui.state.UIState
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOf
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.junit.MockitoJUnit
import org.mockito.kotlin.any

@OptIn(ExperimentalCoroutinesApi::class)
class ProductDetailViewModelTest {
    @get:Rule
    val mockitoRule = MockitoJUnit.rule()

    @get:Rule
    val coroutineRule = CoroutineTestRule()

    @Mock
    lateinit var productDetailUserCase: IUserCase<String, ProductDetailModel>

    @Mock
    lateinit var sharedDataSingleton: SharedDataSingleton

    private lateinit var viewModel: ProductDetailViewModel

    @Before
    fun setup() {
        viewModel = ProductDetailViewModel(productDetailUserCase, sharedDataSingleton)
    }

    @Test
    fun `getProductDetail success`() = coroutineRule.runBlockingTest {
        val mockProductDetail = DummyModel.dummyProductDetailModel

        `when`(
            sharedDataSingleton.getSharedData()
        ).thenReturn(
            dummySharedDataModel
        )

        `when`(
            productDetailUserCase.execute(any())
        ).thenReturn(
            flowOf(listOf(DummyModel.dummyProductDetailModel))
        )

        viewModel.getProductDetail(dummyProductId)

        verify(productDetailUserCase).execute(any())
        verify(sharedDataSingleton, times(2)).getSharedData()

        verifyNoMoreInteractions(productDetailUserCase)

        assert(viewModel.uiState.value is UIState.ProductDetail)
    }

    @Test
    fun `getProductDetail error`() = coroutineRule.runBlockingTest {
        val errorMessage = "Test error message"
        `when`(productDetailUserCase.execute(any())).thenReturn(flow {
            throw Exception(errorMessage)
        })

        viewModel.getProductDetail(dummyProductId)

        verify(productDetailUserCase).execute(any())
        verifyNoMoreInteractions(productDetailUserCase)

        assert(viewModel.uiState.value is UIState.ErrorAppear)
        assert((viewModel.uiState.value as UIState.ErrorAppear).errorMessage == errorMessage)
    }
}
package com.example.myjlpapplication.ui.viewmodel

import com.example.CoroutineTestRule
import com.example.domain.model.DummyModel.dummyProductInfoModelList
import com.example.domain.model.DummyModel.dummySharedDataModel
import com.example.domain.model.ProductInfoModel
import com.example.domain.usercase.IUserCase
import com.example.myjlpapplication.ui.state.UIState
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOf
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito.verify
import org.mockito.Mockito.verifyNoMoreInteractions
import org.mockito.Mockito.`when`
import org.mockito.junit.MockitoJUnit

@OptIn(ExperimentalCoroutinesApi::class)
class ProductListViewModelTest {
    @get:Rule
    val mockitoRule = MockitoJUnit.rule()

    @get:Rule
    val coroutineRule = CoroutineTestRule()

    @Mock
    lateinit var productInfoListUserCase: IUserCase<Unit, ProductInfoModel>

    @Mock
    lateinit var sharedDataSingleton: SharedDataSingleton

    private lateinit var viewModel: ProductListViewModel

    @Before
    fun setup() {
        viewModel = ProductListViewModel(productInfoListUserCase, sharedDataSingleton)
    }

    @Test
    fun `getProductInfoData success`() = coroutineRule.runBlockingTest {
        val mockProductInfo = dummyProductInfoModelList
        `when`(
            productInfoListUserCase.execute(Unit)
        ).thenReturn(
            flowOf(dummyProductInfoModelList)
        )

        viewModel.getProductInfoData()

        verify(productInfoListUserCase).execute(Unit)
        verifyNoMoreInteractions(productInfoListUserCase)

        assert(viewModel.uiState.value is UIState.ProductList)
        assert(
            (viewModel.uiState.value as UIState.ProductList).list == mockProductInfo
        )
    }

    @Test
    fun `getProductInfoData error`() = coroutineRule.runBlockingTest {
        val errorMessage = "Test error message"
        `when`(productInfoListUserCase.execute(Unit)).thenReturn(flow {
            throw Exception(errorMessage)
        })

        viewModel.getProductInfoData()

        verify(productInfoListUserCase).execute(Unit)
        verifyNoMoreInteractions(productInfoListUserCase)

        assert(viewModel.uiState.value is UIState.ErrorAppear)
        assert((viewModel.uiState.value as UIState.ErrorAppear).errorMessage == errorMessage)
    }

    @Test
    fun `updateShareData test`() {
        viewModel.updateShareData(dummySharedDataModel)

        verify(sharedDataSingleton).setSharedData(dummySharedDataModel)
        verifyNoMoreInteractions(sharedDataSingleton)
    }
}
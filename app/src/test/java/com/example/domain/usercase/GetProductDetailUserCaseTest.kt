package com.example.domain.usercase

import com.example.CoroutineTestRule
import com.example.domain.model.DummyModel
import com.example.domain.model.ProductDetailModel
import com.example.domain.repository.IRepository
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flowOf
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnit
import org.mockito.kotlin.any

@OptIn(ExperimentalCoroutinesApi::class)
class GetProductDetailUserCaseTest {
    @get:Rule
    val mockitoRule = MockitoJUnit.rule()

    @get:Rule
    val coroutineRule = CoroutineTestRule()

    @Mock
    lateinit var productDetailRepository: IRepository<String, ProductDetailModel>

    private lateinit var usercase: GetProductDetailUserCase

    @Before
    fun setup() {
        usercase = GetProductDetailUserCase(productDetailRepository)
    }

    @Test
    fun `execute test`() = coroutineRule.runBlockingTest {
        Mockito.`when`(
            productDetailRepository.getData(any())
        ).thenReturn(
            flowOf(listOf(DummyModel.dummyProductDetailModel))
        )

        usercase.execute(DummyModel.dummyProductId)

        Mockito.verify(productDetailRepository).getData(any())
        Mockito.verifyNoMoreInteractions(productDetailRepository)
    }
}
package com.example.domain.usercase

import com.example.CoroutineTestRule
import com.example.domain.model.DummyModel
import com.example.domain.model.ProductInfoModel
import com.example.domain.repository.IRepository
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flowOf
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.junit.MockitoJUnit

@OptIn(ExperimentalCoroutinesApi::class)
class GetProductInfoListUserCaseTest {
    @get:Rule
    val mockitoRule = MockitoJUnit.rule()

    @get:Rule
    val coroutineRule = CoroutineTestRule()

    @Mock
    lateinit var productListRepository: IRepository<Unit, ProductInfoModel>

    private lateinit var usercase: GetProductInfoListUserCase

    @Before
    fun setup() {
        usercase = GetProductInfoListUserCase(productListRepository)
    }

    @Test
    fun `execute test`() = coroutineRule.runBlockingTest {
        `when`(
            productListRepository.getData(Unit)
        ).thenReturn(
            flowOf(DummyModel.dummyProductInfoModelList)
        )

        usercase.execute(Unit)

        verify(productListRepository).getData(Unit)
        verifyNoMoreInteractions(productListRepository)
    }
}